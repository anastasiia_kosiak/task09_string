To the Reader In late 1995, the Java programming language burst onto the Internet
scene and gained instant celebrity status. The promise of Java technology was that it
would become the universal glue that connects users with information wherever it comes
in a unique position to fulfill this promise. It is an extremely solidly engineered
language that has gained wide acceptance. Its built-in security and safety features are
reassuring both to programmers and to the users of Java programs. Java has built-in
support for advanced programming tasks, such as network programming, database
connectivity, and concurrency.
Since 1995, eleven major revisions of the Java Development Kit have been released.
Over the course of the last 20 years, the Application Programming Interface (API) has
grown from about 200 to over 4,000 classes. The API now spans such diverse areas as
user interface construction, database management, each time, we rewrote the book to
take advantage of the newest Java features. This edition has been updated to reflect
the features of Java Standard Edition (SE) 9, 10, and 11. As with the previous
editions of this book, we still target serious programmers who want to put Java to work on real projects. We think of you, our reader,
as a programmer with a solid background in a programming language other than Java,
and we assume that you don’t like books filled with toy examples (such as toasters,
zoo animals, or “nervous text”). You won’t find any of these in our book. Our goal is
to enable you to fully understand the Java language and library, not to give you an
illusion of understanding. the most part, they aren’t fake and they don’t cut corners.
They should make good starting points for your own code. We assume you are willing,
even eager, to learn about all the advanced features that Java puts at your disposal.