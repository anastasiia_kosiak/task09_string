package com.epam.internationalization;
@FunctionalInterface
public interface Printable {
    public void print();
}
