package com.epam.internationalization;

import com.epam.regextask.RegexTest;
import java.util.*;

public class Menu {
    private static final String RES_URI = "com.epam.tasks.internationalization.resources";

    private Map<String,String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private ResourceBundle bundle;
    private Locale locale ;

    public Menu() {

        loadLocaleEn();
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", " 1 - English menu");
        menu.put("2", " 2 - French menu");
        menu.put("3", " 3 - German menu");
        menu.put("4", " 4 - Show regex");
        menu.put("5", " 5 - Split regex(the|you) and replace vowels");
        menu.put("Q", " Q - exit");

        methodsMenu.put("1", this::englishmenu);
        methodsMenu.put("2", this::frenchmenu);
        methodsMenu.put("3", this::germanmenu);
        methodsMenu.put("4", this::showRegex);
        methodsMenu.put("5", this::splitRegex);
    }

    private void loadLocaleEn() {
        locale = new Locale("en", "EN");
        bundle = ResourceBundle.getBundle(RES_URI, locale);
    }

    private void updateLanguage() {
        System.out.println("Name: " + bundle.getObject("name"));
        System.out.println("Lang_menu: " + bundle.getObject("lang_menu"));
        System.out.println("Task: " + bundle.getObject("task"));
        System.out.println("Menu name: " + bundle.getObject("menu_name"));
        System.out.println("Text: " + bundle.getObject("description"));

    }

    private void outputMenu(){
        System.out.println("\n" + bundle.getObject("menu"));

        for (String str: menu.values()){
            System.out.println(str);
        }
    }

    public void show(){
        String keyMenu = null;
        loadLocaleEn();
        do {
            outputMenu();
            System.out.println(bundle.getObject("menu_name"));
            keyMenu = input.nextLine().toUpperCase();
            try{
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){
                System.out.println(bundle.getObject("menu_problem"));
            }
        } while (!keyMenu.equals("Q"));
    }

    private void englishmenu(){
        Locale loc = new Locale("en", "EN");
        bundle = ResourceBundle.getBundle(RES_URI, loc);
        updateLanguage();
    }
    private void frenchmenu(){
        Locale loc = new Locale("fr", "FR");
        bundle = ResourceBundle.getBundle(RES_URI, loc);
        menu.put("2", " 2 - French menu");
        updateLanguage();
    }
    private void germanmenu(){
        Locale loc = new Locale("de", "DE");
        bundle = ResourceBundle.getBundle(RES_URI, loc);
        menu.put("3", " 3 - Germany menu");
        updateLanguage();
    }
    private void showRegex(){
        RegexTest regex = new RegexTest();
        regex.showRegex();
    }

    private void splitRegex(){
        RegexTest regex = new RegexTest();
        regex.splitRegex();
    }
}
