package com.epam.internationalization.resources;

import java.util.ListResourceBundle;

public class EnglishResources extends ListResourceBundle {

    private final Object[][] resources = {
            {"name", "Java Threads Tutorial"},
            {"lang_menu", "English"},
            {"task", "Localization"},
            {"menu_name", "Select menu:"},
            {"menu_problem", "Menu problem"},
            {"menu", "Menu:"},
            {"Description", "Multithreading is a Java feature that allows concurrent execution of two or more parts of a program for maximum utilization of CPU. Each part of such program is called a thread. So, threads are light-weight processes within a process.\n" +
                    "\n" +
                    "Threads can be created by using two mechanisms :\n" +
                    "1. Extending the Thread class\n" +
                    "2. Implementing the Runnable Interface. " +"Thread Class vs Runnable Interface\n" +
                    "\n" +
                    "1. If we extend the Thread class, our class cannot extend any other " +
                    "class because Java doesn’t support multiple inheritance. But, if we " +
                    "implement the Runnable interface, our class can still extend other " +
                    "base classes.\n" +
                    "\n" +
                    "2. We can achieve basic functionality of a thread by extending " +
                    "Thread class because it provides some inbuilt methods like yield(), " +
                    "interrupt() etc. that are not available in Runnable interface."
            }
    };

    @Override
    protected Object[][] getContents() {
        return resources;
    }
}
