package com.epam.bigtask;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Service {
    String fileName = "data.txt";

    private List<Sentence> sentencesObjects;

    private Sentence sentence;
    private List<String> listWithNoSpaces;

    public Service() {
        sentencesObjects = new ArrayList<>();
    }

    public void createAFileWithNoTabs(){

        listWithNoSpaces = new ArrayList<>();
        String regEx = "([A-Z])(.*)[.!?;]";
        Predicate<String> textFilter = Pattern.compile(regEx).asPredicate();

        try (Stream<String> lines2 = Files.lines(Paths.get(fileName))) {
            listWithNoSpaces = lines2
                    .map(line -> line.replaceAll("\\s+|\\t+|\\n", " "))
                    .filter(textFilter)
                    .collect(Collectors.toList());
            Files.write(Paths.get(fileName+1), listWithNoSpaces);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadAllSentences(){
        String[] output = createArrayFromFile();
        String correctPart = null;

        for (String pe : output) {
            correctPart = pe.replaceAll("\\s+|\\t+|\\n+", " ");

            int tmpWordsCount = countWordsUsingSplit(correctPart);
            int tmpDuplicatesCount = findDuplicateWords(correctPart);

            Sentence sentence = new Sentence();
            sentence.setWordsCount(tmpWordsCount);
            sentence.setName(correctPart);
            sentence.setDuplicatesCount(tmpDuplicatesCount);
            sentencesObjects.add(sentence);
        }
    }

    public int findDuplicateWords(String string ) {
        int countDuplicate = 1;
        string = string.toLowerCase();
        String words[] = string.split(" ");
        for (int i = 0; i < words.length; ++i) {
            for (int j = i + 1; j < words.length; ++j) {
                if (words[i].equals(words[j])) {
                    countDuplicate++;
                    words[j] = "0";
                }
            }
        }
        return countDuplicate;
    }
    public int countWordsUsingSplit(String input) {
        if (input == null || input.isEmpty()) {
            return 0;
        }
        String[] words = input.split("\\s+");
        return words.length;
    }

    public String[] createArrayFromFile() {

        String data = "";
        try {
            data = new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] output = data.split("\\.");

        return output;
    }
    public void showSortedWords() {
        Collections.sort(sentencesObjects);
        for(Sentence p: sentencesObjects){
            System.out.println(p.getName());
        }
    }
    public void showAllSentences() {
        for(Sentence p: sentencesObjects){
            System.out.println(p.getName());
        }
    }
    public void showWithDuplicateWords() {
        for(Sentence p: sentencesObjects){
            if(p.getDuplicatesCount()>=2){
                System.out.println(p.getName()+" >");
            }
        }
    }
}
