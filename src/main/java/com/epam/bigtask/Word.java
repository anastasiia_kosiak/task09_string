package com.epam.bigtask;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Word {
    private String name;
    int count;

    private Map<Integer, String> wordsList = new HashMap<>();
    private List<Word> words = new ArrayList<>();

    public Word() { }

    public void countWords() {
        long wordCount = 0;
        Path textFilePath = Paths.get("C:\\JavaAdvanced\\text.txt");
        try {
            Stream<String> fileLines = Files.lines(textFilePath, Charset.defaultCharset());
            wordCount = fileLines.flatMap(line -> Arrays.stream(line.split(" "))).count();
        } catch(IOException ioException) {
            ioException.printStackTrace();
        }
        System.out.println("Number of words text.txt: "+wordCount);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public Map<Integer, String> getWordsFromList() {
        return wordsList;
    }

    public void setWordsList(Map<Integer, String> wordsFromList) {
        this.wordsList = wordsFromList;
    }
}
