package com.epam.bigtask;

public class TaskTest {

    public static Service service;

    public static void main(String[] args) {
        service = new Service();
        service.createAFileWithNoTabs();
        service.loadAllSentences();
        service.showWithDuplicateWords();
    }
}
