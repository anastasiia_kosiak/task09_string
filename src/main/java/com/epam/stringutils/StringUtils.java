package com.epam.stringutils;

import java.lang.reflect.Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class StringUtils {

    private final Logger logger = LogManager.getLogger(StringUtils.class.getName());

    public StringUtils() { }
    public void getClassParametersData(Class classParameter){

        Field[] fields = classParameter.getFields();

        String parameter = "";
        int i = 0;
        for (Field field: fields) {
            if( i%4 == 0){
                parameter += " \n";
            }
            i++;
            parameter += field + "  <<; ";
        }
        logger.info(parameter);

    }
}

