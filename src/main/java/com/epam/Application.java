package com.epam;

import com.epam.internationalization.Menu;
import com.epam.stringutils.*;

public class Application {
    public static void main(String[] args) {
        StringUtils stringUtils = new StringUtils();
        stringUtils.getClassParametersData(FirstExample.class);
        stringUtils.getClassParametersData(SecondExample.class);

        Menu menu = new Menu();
        menu.show();
    }
}
